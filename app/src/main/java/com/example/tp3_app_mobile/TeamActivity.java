package com.example.tp3_app_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TeamActivity extends AppCompatActivity {
    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;
    private ImageView imageBadge;
    private Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);


        Intent intent = getIntent();
        team = intent.getParcelableExtra("equipe");


        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textTeamName.setText(team.getName());

        textLeague = (TextView) findViewById(R.id.league);
        textLeague.setText(team.getLeague());

        textStadium = (TextView) findViewById(R.id.stadium);
        textStadium.setText(team.getStadium());

        textStadiumLocation = (TextView) findViewById(R.id.location);
        textStadiumLocation.setText(team.getStadiumLocation());

        textTotalScore = (TextView) findViewById(R.id.score);
        //textTotalScore.setText(team.getTotalPoints());

        textRanking = (TextView) findViewById(R.id.rank);
        //textRanking.setText(team.getRanking());

        textLastMatch = (TextView) findViewById(R.id.lastMatch);
        //textLastMatch.setText((CharSequence) team.getLastEvent());

        textLastUpdate = (TextView) findViewById(R.id.lastUpdate);
        textLastUpdate.setText(team.getLastUpdate());

        //imageBadge = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.buttonAdd);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO

            }
        });
    }


    @Override
    public void onBackPressed() {
        //TODO : prepare result for the main activity
        super.onBackPressed();
    }

    private void updateView() {

        /*textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());*/

        //TODO : update imageBadge
    }
}
