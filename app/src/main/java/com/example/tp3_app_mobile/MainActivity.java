package com.example.tp3_app_mobile;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.SimpleTimeZone;

public class MainActivity extends AppCompatActivity {
    Team team ;
    SimpleCursorAdapter cursorAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final SportDbHelper dbHelper = new SportDbHelper(this);
        //ouvre BDD en lecture
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        dbHelper.populate();

//        cursorAdapter = new SimpleCursorAdapter(
//                MainActivity.this,android.R.layout.simple_list_item_2,result,
//                new String[]{WineDbHelper.COLUMN_NAME,WineDbHelper.COLUMN_LOC},new int[]{android.R.id.text1,android.R.id.text2},0);
//        //listview récup by id dans le layout
//        final ListView lv = (ListView) findViewById(R.id.listWine);
//        lv.setAdapter(cursorAdapter);
//        db.close();

        RecyclerView teamRecyler = (RecyclerView) findViewById(R.id.recyclerTeam);


        cursorAdapter = new SimpleCursorAdapter(
                MainActivity.this,
                android.R.layout.simple_list_item_2,dbHelper.fetchAllTeams(),
                new String[]{SportDbHelper.COLUMN_TEAM_NAME,SportDbHelper.COLUMN_LEAGUE_NAME},
                new int []{android.R.id.text1, android.R.id.text2},0);


        final ListView lv = (ListView) findViewById(R.id.listTeam);
        lv.setAdapter(cursorAdapter);
        db.close();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Cursor curs =  (Cursor) parent.getItemAtPosition(position);
                Team team = dbHelper.cursorToTeam(curs);
                curs.moveToFirst();
                Intent intent = new Intent(MainActivity.this, TeamActivity.class);
                //intent on envoi la team concerné
                Toast.makeText(MainActivity.this, "You selected:" + team,
                        Toast.LENGTH_LONG).show();
                intent.putExtra("equipe", team);
                //lancement intent
                startActivityForResult(intent,1);

            }
        });


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewTeamActivity.class);
                startActivity(intent);
            }
        });

        ItemTouchHelper.SimpleCallback itemTouch = new ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT)
        {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                dbHelper.deleteTeam(viewHolder.getAdapterPosition());
//                Log.i(TAG, "onSwiped: suppression ok "+viewHolder.getAdapterPosition());
                dbHelper.close();
            }
        };

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
