package com.example.tp3_app_mobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class TestTeam extends AppCompatActivity {

    Team team;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_team);

        Intent intent = getIntent();
        team = intent.getParcelableExtra("equipe");

        TextView textTeamName = (TextView) findViewById(R.id.nameTeam);
        textTeamName.setText(team.getName());
    }
}
