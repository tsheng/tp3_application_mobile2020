package com.example.tp3_app_mobile;

import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    TextView teamName=null;
    TextView ligueName=null;
    //ImageView imageViewRow=null;

    RowHolder(View row)
    {
        super(row);
        teamName=(TextView) row.findViewById(R.id.teamName);
        ligueName=(TextView) row.findViewById(R.id.ligueTeam);
        //imageViewRow=(ImageView) row.findViewById(R.id.imageTeam);
        row.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        final String team = teamName.getText().toString();
        final String ligue = ligueName.getText().toString();

        Intent myIntent= new Intent(v.getContext(),TeamActivity.class);
        myIntent.putExtra("equipe", team);
        v.getContext().startActivity(myIntent);
    }

    public void bindModel(String text, int idImage) {
        teamName.setText(text);
        ligueName.setText(text);
        //imageViewRow.setImageResource(idImage);
    }
}
