package com.example.tp3_app_mobile;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;


public class NewTeamActivity extends AppCompatActivity {

    private EditText textTeam, textLeague;
    SportDbHelper dbHelper = new SportDbHelper(this);
    @Override
    protected void onCreate(Bundle instance)
    {
        super.onCreate(instance);
        setContentView(R.layout.activity_new_team);

        Intent intent = getIntent();
        textTeam = (EditText) findViewById(R.id.editNewName);
        textLeague = (EditText) findViewById(R.id.editNewLeague);

        final Button but = (Button) findViewById(R.id.buttonAdd);

        but.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewTeamActivity.this , MainActivity.class);
                Team team = new Team(textTeam.getText().toString(),
                        textLeague.getText().toString()
                );


                if (NewTeamActivity.this.textTeam.getText().toString().isEmpty())
                {
                    new AlertDialog.Builder(NewTeamActivity.this)
                            .setTitle("Sauvegarde impossible")
                            .setMessage("Le nom de l'équipe doit être non vide.")
                            .show();
                }
                else
                 {
                    intent.putExtra("equipe", team);
                    dbHelper.addTeam(team);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }


}
